document.addEventListener('DOMContentLoaded', function() {
    var pageTop = document.getElementById("page-top");
    if (pageTop !== null) {

        /* ----------------------------- add home button ---------------------------- */

        var homeButtonWarrper = document.querySelector("#main_navigation_collapse .nav:nth-child(2)");
        var newNavWithHomeButton = "<li><a id='js_home_button' href='/'>Home</a></li>" + homeButtonWarrper.innerHTML;
        homeButtonWarrper.innerHTML = newNavWithHomeButton;
       

        /* ---------------------------  end adding home --------------------------- */
    }
});
